{ pkgs ? import <nixpkgs> {} }:
pkgs.mkShell {
  buildInputs = [ pkgs.stack
                  pkgs.gcc
                  pkgs.zlib
                  pkgs.gmp
                  pkgs.pkg-config
                  pkgs.xorg.libX11
                  pkgs.xorg.libXrandr
                  pkgs.xorg.libXrender
                  pkgs.xorg.libXinerama
                  pkgs.xorg.libXScrnSaver
                  pkgs.xorg.libXft
                  pkgs.xorg.libXext
                ];
}
